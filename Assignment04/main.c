#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/printk.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phuti Matle <pmatle@student.wethinkcode.co.za>");
MODULE_DESCRIPTION("Hello World module");

static int __init hello_init(void)
{
	pr_info("Hello World !\n");
	return (0);
}

static void __exit hello_exit(void)
{
	pr_notice("Cleaning up module.\n");
}

module_init(hello_init);
module_exit(hello_exit);
