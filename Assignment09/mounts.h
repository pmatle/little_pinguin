#ifndef MOUNTS_H
# define MOUNTS_H

# include <linux/module.h>
# include <linux/kernel.h>
# include <linux/init.h>
# include <linux/printk.h>
# include <linux/proc_fs.h>
# include <linux/fs_struct.h>
# include <linux/mount.h>
# include <linux/fs.h>
# include <linux/path.h>
# include <linux/string.h>
# include <linux/namei.h>
# include <linux/slab.h>
# include <linux/kallsyms.h>
# include <asm/uaccess.h>
# include <asm/page.h>

static ssize_t		mymounts_read(struct file *fp, char __user *buff,
		size_t size, loff_t *offs);
static int		mymounts_open(struct inode *inode, struct file *file);
static int		mymounts_release(struct inode *inode,
		struct file *file);
static int		(*iterate)(int (*f)(struct vfsmount *, void *),
		void *args, struct vfsmount *root);
static struct vfsmount	*(*collect)(struct path *path);
static int		write_entry(struct vfsmount *mount, void *data);

#endif
