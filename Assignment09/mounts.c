// SPDX-License-Identifier: GPL-2.0
#include "mounts.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phuti Matle <pmatle@student.wethinkcode.co.za>");
MODULE_DESCRIPTION("A module to list mount points");

static const struct file_operations	fops = {
	.owner = THIS_MODULE,
	.read = mymounts_read,
	.open = mymounts_open,
	.release = mymounts_release
};

static struct proc_dir_entry	*mymounts;
static char	*file_contents;

static int __init hello_init(void)
{
	mymounts = proc_create("mymounts", 0644, NULL, &fops);
	if (!mymounts)
		return -1;
	pr_info("%s\n", file_contents);
	return 0;
}

static int	mymounts_open(struct inode *inode, struct file *file)
{
	struct path	path;
	struct vfsmount	*mount;

	kern_path("/", LOOKUP_FOLLOW, &path);
	iterate = (void *)kallsyms_lookup_name("iterate_mounts");
	collect = (void *)kallsyms_lookup_name("collect_mounts");
	mount = collect(&path);
	iterate(write_entry, NULL, mount);
	return 0;
}

static char	*create_entry(char *holder, char *name, char *path)
{
	if (name[0] == '\0')
		holder = strncat(holder, "root", 4);
	else
		holder = strncat(holder, name, strlen(name));
	holder = strncat(holder, "\t\t", 2);
	holder = strncat(holder, path, strlen(path));
	holder = strncat(holder, "\n", 1);
	return holder;
}

static int		add_contents(char *mount_name, char *mount_path)
{
	size_t	len;
	char	*tmp;

	len = strlen(mount_name) + strlen(mount_path) + 4;
	if ((ksize(file_contents) - strlen(file_contents)) < len) {
		tmp = kzalloc(sizeof(*tmp) * (ksize(file_contents) + len),
			      GFP_KERNEL);
		if (!tmp)
			return 0;
		tmp = strncat(tmp, file_contents, strlen(file_contents));
		kfree(file_contents);
		file_contents = tmp;
	}
	file_contents = create_entry(file_contents, mount_name, mount_path);
	return 1;
}

static int		write_entry(struct vfsmount *mount, void *data)
{
	struct path	path;
	char		*mount_path;
	char		*mount_name;
	char		buff[50];

	if (!file_contents) {
		file_contents = kzalloc(sizeof(*file_contents) * PAGE_SIZE,
					GFP_KERNEL);
		if (!file_contents)
			return 0;
	}
	path.mnt = mount;
	path.dentry = mount->mnt_root;
	mount_path = d_path(&path, buff, 50);
	mount_name = strrchr(mount_path, '/') + 1;
	if (!add_contents(mount_name, mount_path))
		return 1;
	return 0;
}

static ssize_t		mymounts_read(struct file *fp, char __user *buff,
				      size_t size, loff_t *offs)
{
	int	ret;
	char	*tmp;

	if (!buff)
		return -EFAULT;
	tmp = file_contents;
	ret = simple_read_from_buffer(buff, size, offs, tmp, strlen(tmp));
	return ret;
}

static int		mymounts_release(struct inode *inode, struct file *fp)
{
	kfree(file_contents);
	return 0;
}

static void __exit hello_exit(void)
{
	pr_notice("Cleaning up module.\n");
	proc_remove(mymounts);
}

module_init(hello_init);
module_exit(hello_exit);
