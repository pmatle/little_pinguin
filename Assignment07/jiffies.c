#include "main.h"

static char	*contents_ptr;
static int	ptr_pos;

static struct	file_operations jiffies_fops = {
	.read = jiffies_read,
	.open = jiffies_open,
	.release = jiffies_release
};

int		create_jiffies_file(struct dentry *folder)
{
	struct dentry	*jiffies_file;

	jiffies_file = debugfs_create_file("jiffies", 0444, folder, NULL,
			&jiffies_fops);	
	pr_info("debufs : Created the jiffies virtual file successfully\n");
	if (jiffies_file == (ERR_PTR(-ENODEV)) || jiffies_file == NULL)
		return (print_err(jiffies_file, "file"));
	return (0);
}

int		jiffies_open(struct inode *inode, struct file *file)
{
	int	len;

	ptr_pos = 0;
	len = snprintf(0, 0, "%ld", jiffies);
	contents_ptr = kmalloc((sizeof(*contents_ptr) * (len + 1)), GFP_KERNEL);
	if (contents_ptr == NULL)
		return (-EAGAIN);
	snprintf(contents_ptr, len, "%ld", jiffies);
	return (0);
}

ssize_t		jiffies_read(struct file *file, char __user *buff, size_t size,
		loff_t *offset)
{
	int	bytes_read;

	bytes_read = 0;
	if (buff == NULL)
		return (-EFAULT);
	if (contents_ptr[ptr_pos] == '\0')
		return (0);
	while (size && contents_ptr[ptr_pos] != '\0')
	{
		put_user(contents_ptr[ptr_pos], buff);
		buff++;
		ptr_pos++;
		size--;
		bytes_read++;
	}
	put_user('\0', buff);
	return (bytes_read);
}

int		jiffies_release(struct inode *inode, struct file *file)
{
	kfree(contents_ptr);
	return (0);
}
