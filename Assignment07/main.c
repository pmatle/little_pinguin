#include "main.h"

static struct	dentry *folder;

static int	__init debugfs_init(void)
{
	int	ret;

	pr_info("Initializing the debugfs module...\n");
	folder = debugfs_create_dir("fortytwo", NULL);
	if (folder == (ERR_PTR(-ENODEV)) || folder == NULL)
		return (print_err(folder, "folder"));
	ret = create_id_file(folder);
	if (ret != 0)
		goto cleanup_dir;
	ret = create_jiffies_file(folder);
	if (ret != 0)
		goto cleanup_dir;
	ret = create_foo_file(folder);
	if (ret != 0)
		goto cleanup_dir;
	return (0);
cleanup_dir:
	debugfs_remove_recursive(folder);
	return (ret);
}

static void	__exit debugfs_exit(void)
{
	pr_notice("Cleaning up the debugfs module.\n");
	debugfs_remove_recursive(folder);
}

module_init(debugfs_init);
module_exit(debugfs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phuti Matle <pmatle@student.wethinkcode.co.za>");
MODULE_DESCRIPTION("debugfs module.");
MODULE_VERSION("1.0.0");
