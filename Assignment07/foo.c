#include "main.h"

static		DEFINE_MUTEX(foo_mutex);

struct	my_data {
	char	file_contents[PAGE_SIZE + 1];
	int	read_pos;
}		my_data;

static struct	my_data data;

static struct	file_operations foo_fops= {
	.owner = THIS_MODULE,
	.read = foo_read,
	.write = foo_write,
	.open = foo_open,
	.release = foo_release
};

int		create_foo_file(struct dentry *folder)
{
	static struct	dentry *foo_file;

	foo_file = debugfs_create_file_size("foo", 0644, folder, NULL,
			&foo_fops, PAGE_SIZE);
	if (foo_file == (ERR_PTR(-ENODEV)) || foo_file == NULL)
		return (print_err(foo_file, "file"));
	pr_info("debugfs: Created the foo virtual file successfully\n");
	mutex_init(&foo_mutex);
	return (0);
}

int		foo_open(struct inode *inode, struct file *file)
{
	if (file->f_mode & FMODE_WRITE) {
		if (!mutex_trylock(&foo_mutex)) {
			pr_alert("debugfs: file id in use\n");
			return (-EBUSY);
		}
	}
	pr_info("debugfs: foo file opened successfully\n");
	data.read_pos = 0;
	return (0);
}

ssize_t		foo_read(struct file *file, char __user *buff, size_t size,
		loff_t *offset)
{
	int	ret;

	if (buff == NULL)
		return (-EFAULT);
	*offset = data.read_pos;
	ret = simple_read_from_buffer(buff, size, offset, data.file_contents,
			PAGE_SIZE);
	data.read_pos = *offset;
	return (ret);
}

ssize_t		foo_write(struct file *file, const char __user *buff,
		size_t size, loff_t *offset)
{
	int	ret;

	if (buff == NULL)
		return (-EFAULT);
	if (file->f_flags & O_APPEND) {
		*offset = strlen(data.file_contents);
	} else {
		*offset = 0;
		memset(data.file_contents, '\0',strlen(data.file_contents));
	}
	ret = simple_write_to_buffer(data.file_contents, PAGE_SIZE, offset,
			buff, size);
	return (ret);
}

int		foo_release(struct inode *inode, struct file *file)
{
	pr_info("debugfs: foo file released successfully\n");
	mutex_unlock(&foo_mutex);
	return (0);
}
