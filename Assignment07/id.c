#include "main.h"

static		DEFINE_MUTEX(id_mutex);
static char	contents[7] = "pmatle\n";
static char	*contents_ptr;

static struct	file_operations id_fops = {
	.owner = THIS_MODULE,
	.read = id_read,
	.write = id_write,
	.open = id_open,
	.release = id_release
};

int		create_id_file(struct dentry *folder)
{
	static struct	dentry *id_file;

	id_file = debugfs_create_file("id", 0666, folder, NULL, &id_fops);
	if (id_file == (ERR_PTR(-ENODEV)) || id_file == NULL)
		return (print_err(id_file, "file"));	
	pr_info("debugfs: Created the id virtual file successfully\n");
	mutex_init(&id_mutex);
	return (0);
}

int		id_open(struct inode *inode, struct file *file)
{
	if (!mutex_trylock(&id_mutex)) {
		pr_alert("debugfs : file id in use by another process\n");
		return (-EBUSY);
	}
	contents_ptr = contents;
	return (0);
}

ssize_t		id_read(struct file *fp, char __user *buff, size_t size,
		loff_t *offset)
{
	int	bytes_read;

	bytes_read = 0;
	if (buff == NULL)
		return (-EFAULT);
	if (*contents_ptr == 0)
		return (0);
	while (size && *contents_ptr)
	{
		put_user(*contents_ptr, buff);
		contents_ptr++;
		buff++;
		size--;
		bytes_read++;
	}
	return (bytes_read);
}

ssize_t		id_write(struct file *fp, const char __user *buff, size_t size,
		loff_t *offset)
{
	if (buff == NULL)
		return (-EFAULT);
	if (buff && strcmp(buff, "pmatle\n") == 0) {
		return (7);
	}
	return (-EINVAL);
}

int		id_release(struct inode *inode, struct file *file)
{
	mutex_unlock(&id_mutex);
	return (0);
}
