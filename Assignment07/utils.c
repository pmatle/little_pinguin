#include "main.h"

size_t	print_err(struct dentry *ret, char *type)
{
	if (ret == (ERR_PTR(-ENODEV)))
	{
		pr_notice("debugfs: CONFIG_DEBUG_FS not enabled in kernel\n");
		return (-ENODEV);
	}
	pr_notice("Error occurred when creating %s\n", type);
	return (-1);
}
