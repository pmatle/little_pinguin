#ifndef MAIN_H
# define MAIN_H

# include <linux/module.h>
# include <linux/kernel.h>
# include <linux/init.h>
# include <linux/printk.h>
# include <linux/fs.h>
# include <linux/uaccess.h>
# include <linux/debugfs.h>
# include <linux/string.h>
# include <linux/mutex.h>
# include <linux/jiffies.h>
# include <linux/slab.h>
# include <linux/fcntl.h>
# include <asm/page.h>

size_t	print_err(struct dentry *ret, char *type);
int	create_id_file(struct dentry *folder);
int	create_jiffies_file(struct dentry *folder);
int	create_foo_file(struct dentry *folder);
int	id_open(struct inode *inode, struct file *file);
int	id_release(struct inode *inode, struct file *file);
ssize_t	id_read(struct file *file, char __user *buff, size_t size,
		loff_t *offset);
ssize_t	id_write(struct file *file, const char __user *buff,
		size_t size, loff_t *offset);
int	jiffies_open(struct inode *inode, struct file *file);
int	jiffies_release(struct inode *inode, struct file *file);
ssize_t	jiffies_read(struct file *file, char __user *buff, size_t size,
		loff_t *offset);

int	foo_open(struct inode *inode, struct file *file);
int	foo_release(struct inode *inode, struct file *file);
ssize_t	foo_read(struct file *file, char __user *buff, size_t size,
		loff_t *offset);
ssize_t	foo_write(struct file *file, const char __user *buff,
		size_t size, loff_t *offset);

#endif
