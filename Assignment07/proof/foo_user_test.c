#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

int	main(void)
{
	int	fd;
	char	buff[6];

	fd = open("/sys/kernel/debug/fortytwo/foo", O_RDONLY);
	if (fd == -1)
		printf("Error: unable to open file\n");
	while (read(fd, buff, 6) > 0)
	{
		printf("%s", buff);
	}
	printf("\n");
	return (0);
}
