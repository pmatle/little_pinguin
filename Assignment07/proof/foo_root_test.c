#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

int	main(void)
{
	int	fd;
	int	ret;

	fd = open("/sys/kernel/debug/fortytwo/foo", O_RDWR | O_APPEND);
	if (fd == -1)
		printf("Error: unable to open file\n");
	ret = write(fd, "pmatle is awesome. ", 19);
	if (ret == -1 && errno == EINVAL)
		printf("Error: unable to write to fd\n");
	else if (ret > 0)
		printf("Wrote %d bytes to fd successfully\n", ret);
	ret = write(fd, "I know right!", 13);
	if (ret == -1 && errno == EINVAL)
		printf("Error: unable to write to fd\n");
	else if (ret > 0)
		printf("Wrote %d bytes to fd successfully\n", ret);
	ret = close(fd);
	if (ret == -1)
		printf("Error: unable to close from fd\n");
	return (0);
}
