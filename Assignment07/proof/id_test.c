#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

int	main(void)
{
	int	fd;
	char	buff[6];
	int	ret;

	fd = open("/sys/kernel/debug/fortytwo/id", O_RDWR);
	if (fd == -1)
		printf("Error: unable to open file\n");

	ret = read(fd, buff, 6);
	printf("%d\n", ret);
	if (ret == -1)
		printf("Error: unable to read from fd\n");
	else
		printf("String : %s\n", buff);

	ret = write(fd, "pmatle is awesome", 17);
	if (ret == -1 && errno == EINVAL)
		printf("Error: unable to write to fd\n");

	ret = write(fd, "pmatle", 6);
	if (ret == -1 && errno == EINVAL)
		printf("Error: unable to write to fd\n");
	else if (ret > 0)
		printf("Wrote %d bytes to fd successfully\n", ret);
	
	ret = close(fd);
	if (ret == -1)
		printf("Error: unable to close from fd\n");
	return (0);
}
