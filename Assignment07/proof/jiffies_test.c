#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int	main(void)
{
	int	fd;
	char	buff[100];
	int	ret;

	fd = open("/sys/kernel/debug/fortytwo/jiffies", O_RDONLY);
	if (fd == -1)
		printf("Error: unable to open file\n");
	ret = read(fd, buff, 100);
	if (ret == -1)
		printf("Error: unable to read from fd\n");
	else
		printf("String : %s\n", buff);
	printf("Bytes read : %d\n", ret);
	bzero(buff, sizeof(buff));
	ret = read(fd, buff, 100);
	if (ret == -1)
		printf("Error: unable to read from fd\n");
	printf("Bytes read : %d\n", ret);
	ret = close(fd);
	if (ret == -1)
		printf("Error: unable to close from fd\n");
	return (0);
}
