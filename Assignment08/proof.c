#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int	read_device(void)
{
	char	buff[100];
	int	fd;

	if ((fd = open("/dev/reverse", O_RDWR | O_APPEND)) == -1) {
		printf("Error: Could not open file id\n");
		return (-1);
	}
	if (read(fd, buff, 11) == -1) {
		printf("Error: could not read file id\n");
		return (-1);
	}
	printf("%s", buff);
	bzero(buff, 100);
	if (read(fd, buff, 17) == -1) {
		printf("Error: could not read file id\n");
		return (-1);
	}
	printf("%s\n", buff);
	bzero(buff, 100);
	if (read(fd, buff, 12) == 0) {
		printf("Done reading\n");
	}
	close(fd);
	return (0);
}

int	write_device(void)
{
	int	fd;

	if ((fd = open("/dev/reverse", O_RDWR | O_APPEND)) == -1) {
		printf("Error: Could not open file id\n");
		return (-1);
	}
	if (write(fd, "Hello World", 11) == -1) {
		printf("Error: could not read file id\n");
		return (-1);
	}
	if (write(fd, " Phuti is Awesome", 17) == -1) {
		printf("Error: could not read file id\n");
		return (-1);
	}
	close(fd);
	return (0);
}

int	main(void)
{
	if (write_device() == -1)
		return (-1);
	if (read_device() == -1)
		return (-1);
	return (0);
}
