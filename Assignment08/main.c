// SPDX-License-Identifier: GPL-2.0
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <asm/page.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Louis Solofrizzo <louis@ne02ptzero.me>");
MODULE_DESCRIPTION("Useless module");

static ssize_t	myfd_read(struct file *fp, char __user *user, size_t size,
			  loff_t *offs);
static ssize_t	myfd_write(struct file *fp, const char __user *user,
			   size_t size, loff_t *offs);

static const struct file_operations	myfd_fops = {
	.owner = THIS_MODULE,
	.read = myfd_read,
	.write = myfd_write
};

static struct miscdevice	myfd_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "reverse",
	.fops = &myfd_fops,
	.mode = 0666
};

char	str[PAGE_SIZE + 1];

static int __init myfd_init(void)
{
	int	retval;

	retval = misc_register(&myfd_device);
	return retval;
}

static void __exit myfd_cleanup(void)
{
	misc_deregister(&myfd_device);
}

static char	*ft_strrev(char *str)
{
	char	c;
	int	x;
	int	len;
	int	center;

	x = 0;
	len = strlen(str);
	center = len / 2;
	while (x < center) {
		c = str[x];
		str[x] = str[len - 1];
		str[len - 1] = c;
		x++;
		len--;
	}
	return str;
}

static ssize_t	myfd_read(struct file *fp, char __user *user, size_t size,
			  loff_t *offs)
{
	int	ret;
	char	*tmp;

	if (!user)
		return -EFAULT;
	if (strlen(str) == 0)
		return 0;
	tmp = str;
	tmp = ft_strrev(tmp);
	ret = simple_read_from_buffer(user, size, offs, tmp, PAGE_SIZE);
	tmp = ft_strrev(tmp);
	return ret;
}

static ssize_t	myfd_write(struct file *fp, const char __user *user,
			   size_t size, loff_t *offs)
{
	int	ret;

	if (!user)
		return -EFAULT;
	if (fp->f_flags & O_APPEND) {
		*offs = strlen(str);
	} else {
		*offs = 0;
		memset(str, '\0', strlen(str));
	}
	ret = simple_write_to_buffer(str, PAGE_SIZE, offs, user, size);
	return ret;
}

module_init(myfd_init);
module_exit(myfd_cleanup);
