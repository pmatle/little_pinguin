// SPDX-License-Identifier: GPL-2.0
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/printk.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/string.h>

static		DEFINE_MUTEX(fortytwo_mutex);
static char	*contents_ptr;
static char	contents[6] = "pmatle";

static int	misc_dev_open(struct inode *, struct file *);
static int	misc_dev_release(struct inode *, struct file *);
static ssize_t	misc_dev_read(struct file *fp, char *buff, size_t size,
			      loff_t *offset);
static ssize_t	misc_dev_write(struct file *fp, const char *buff, size_t size,
			       loff_t *offset);

static const struct	file_operations fops = {
	.owner = THIS_MODULE,
	.read = misc_dev_read,
	.write = misc_dev_write,
	.open = misc_dev_open,
	.release = misc_dev_release
};

static struct	miscdevice misc_dev_fortytwo = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "fortytwo",
	.fops = &fops,
	.mode = 0666
};

static int __init misc_driver_init(void)
{
	int	ret;

	pr_info("Initializing the fortytwo misc char device\n");
	ret = misc_register(&misc_dev_fortytwo);
	if (ret) {
		pr_err("failed to register misc driver driver fortytwo\n");
		return ret;
	}
	pr_info("fortytwo misc char device initialized\n");
	mutex_init(&fortytwo_mutex);
	return 0;
}

static int	misc_dev_open(struct inode *inode, struct file *file)
{
	if (!mutex_trylock(&fortytwo_mutex)) {
		pr_alert("fortytwo: Device in use by another process\n");
		return -EBUSY;
	}
	contents_ptr = contents;
	pr_info("fortytwo: Device opened successfully\n");
	return 0;
}

static ssize_t	misc_dev_read(struct file *fp, char *buff, size_t size,
			      loff_t *offset)
{
	int	bytes_read;
	int	error;

	bytes_read = 0;
	if (!buff)
		return -EFAULT;
	if (*contents_ptr == 0)
		return 0;
	while (size && *contents_ptr) {
		error = put_user(*contents_ptr, buff);
		if (error)
			return (-EFAULT);
		contents_ptr++;
		buff++;
		size--;
		bytes_read++;
	}
	return bytes_read;
}

static ssize_t	misc_dev_write(struct file *fp, const char *buff, size_t size,
			       loff_t *offset)
{
	if (buff && (strcmp(buff, "pmatle") == 0))
		return 6;
	return -EINVAL;
}

static int	misc_dev_release(struct inode *inode, struct file *file)
{
	mutex_unlock(&fortytwo_mutex);
	pr_info("fortytwo: Device closed successfully\n");
	return 0;
}

static void	__exit misc_driver_exit(void)
{
	mutex_destroy(&fortytwo_mutex);
	pr_notice("Cleaning up the fortytwo misc char driver.\n");
	misc_deregister(&misc_dev_fortytwo);
}

module_init(misc_driver_init);
module_exit(misc_driver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phuti Matle <pmatle@student.wethinkcode.co.za>");
MODULE_DESCRIPTION("misc char device driver.");
MODULE_VERSION("1.0.0");
